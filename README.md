
## Source

Releases:

https://ftp.mozilla.org/pub/firefox/releases/

## Source Homebrew Compilation

https://ftp.mozilla.org/pub/firefox/releases/78.15.0esr/source/firefox-78.15.0esr.source.tar.xz



## Current / Unstable 


https://ftp.mozilla.org/pub/firefox/releases/96.0b5/source/

firefox-96.0b5.source.tar.xz



## Quick Steps 


https://firefox-source-docs.mozilla.org/setup/linux_build.html


Quick Install Steps:

Step 1

sudo apt-get update -y

Step 2

sudo apt-get install -y mach


## MACH


mach build


Requirements
Mach requires a current version of mozilla-central (or a tree derived from) (mach was committed on 2012-09-26). Mach also requires Python 2.7. mach itself is Python 3 compliant. However, modules used by mach likely aren't Python 3 compliant just yet. Stick to Python 2.7.

Running
From the root of the source tree checkout, you should just be able to type:

$ ./mach
If all is well, you should see a help message.

For full help:

$ ./mach help
Try building the tree:

$ ./mach build
If you get error messages, make sure that you have all of the build requisites for your system.



Requirements
Mach requires a current version of mozilla-central (or a tree derived from) (mach was committed on 2012-09-26). Mach also requires Python 2.7. mach itself is Python 3 compliant. However, modules used by mach likely aren't Python 3 compliant just yet. Stick to Python 2.7.

Running
From the root of the source tree checkout, you should just be able to type:

$ ./mach
If all is well, you should see a help message.

For full help:

$ ./mach help
Try building the tree:

$ ./mach build
If you get error messages, make sure that you have all of the build requisites for your system.

If it works, you can look at compiler warnings:

$ ./mach warnings-list
Try running some tests:

$ ./mach xpcshell-test services/common/tests/unit/
Or run an individual test:

$ ./mach mochitest browser/base/content/test/general/browser_pinnedTabs.js
You run mach from the source directory, so you should be able to use your shell's tab completion to tab-complete paths to tests. Mach figures out how to execute the tests for you!

mach and mozconfigs
It's possible to use mach with multiple mozconfig files. mach's logic for determining which mozconfig to use is effectively the following:

If a .mozconfig file exists in the current directory, use that.
If the MOZCONFIG environment variable is set, use the file pointed to in that variable.
If the current working directory mach is invoked with is inside an object directory, the mozconfig used when creating that object directory is used.
The default mozconfig search logic is applied.
Here are some examples:

Use an explicit mozconfig file.
$ MOZCONFIG=/path/to/mozconfig ./mach build

Alternatively (for persistent mozconfig usage):
$ export MOZCONFIG=/path/to/mozconfig
$ ./mach build

Let's pretend the MOZCONFIG environment variable isn't set. This will use
the mozconfig from the object directory.
$ cd objdir-firefox
$ mach build
Adding mach to your shell's search path
If you add mach to your path (by modifying the PATH environment variable to include your source directory, or by copying mach to a directory in the default path like /usr/local/bin) then you can type mach anywhere in your source directory or your objdir.  Mach expands relative paths starting from the current working directory, so you can run commands like mach build . to rebuild just the files in the current directory.  For example:

$ cd browser/devtools
$ mach build webconsole # Rebuild only the files in the browser/devtools/webconsole directory
$ mach mochitest-browser webconsole/test # Run browser-chrome tests from browser/devtools/webconsole/test
Enable tab completion
To enable tab completion in bash, run the following command.  You can add the command to your .profile so it will run automatically when you start the shell:

source /path/to/mozilla-central/python/mach/bash-completion.sh
This will enable tab completion of mach command names, and in the future it may complete flags and other arguments too.  Note: Mach tab completion will not work when running mach in a source directory older than Firefox 24.

For zsh, you can call the built-in bashcompinit function before sourcing:

autoload bashcompinit
bashcompinit
source /path/to/mozilla-central/python/mach/bash-completion.sh
Frequently Asked Questions
Why should I use mach?
You should use mach because it provides a better and more unified developer experience for working on Mozilla projects. If you don't use mach, you have to find another solution for the following problems:

Discovering what commands or make targets are available (mach exposes everything through mach help)
Making more sense out of command output (mach offers terminal colorization and structured logging)
Getting productive tools in the hands of others (mach advertises tools to people through mach help - people don't need to discover your tool from a blog post, wiki page, or word of mouth)





## JITSI 

>==== 
- On deb http://mirrordirector.raspbian.org/raspbian/ stretch main contrib non-free rpi, i.e. 4.9.41-v7+ #1023 SMP Tue Aug 8 16:00:15 BST 2017 armv7l GNU/Linux  

   Firefox:  Version 60.0.3112.89 (Developer Build) Built on Ubuntu 14.04, running on Raspbian 9.1 (32-bit) 
   ii  chromium-browser                      60.0.3112.89-0ubuntu0.14.04.1.1010, armhf 
   Gitlab browser, non working.


   stretch 52.x on retropie 4.5.x 

==== 
 ASCII : ii  firefox-esr  68.10.0esr-1~deb9u1                amd64        Mozilla Firefox web browser - Extended Support Release (ESR)  
 Gitlab working, but meet jitsi is not working.

==== 
 Leap OpenSuse:  Working on Leap OpenSuse, with firefox 78.10 esr 64 bits, OpenSuse 15.3 

Gitlab working + meet jitsi is not working.
>==== 


















